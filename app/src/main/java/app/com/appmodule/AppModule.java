package app.com.appmodule;

import android.app.Activity;


/**
 * <p>Класс - модуль для работы с приложением</p>
 * <p>С его помощью будет возможность взаимодействовать с приложением</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 */
public class AppModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "APP_MODULE";

    Activity activity;

    public AppModule(Activity _activity){
        activity = _activity;
    }

    /**
     * <p>Открывает интент по юри (может быть почта, может быть урл в браузер)</p>
     * @param uri Путь передаваемый интенту
     */
    private void loadUrl(String uri){
        //TO DO
    }

    /**
     * <p>Возвращает локализацию в виде ru-RU, en-US</p>
     */
    private String getLocale(){
        //TO DO
        return "";
    }

    /**
     * <p>Посылает интент указанному пакету с указанным юри</p>
     * @param pack Имя пакета приложения в который нужно отправить интент
     * @param uri Путь передаваемый интенту
     */
    public boolean openInApp(String pack, String uri){
        //TO DO
        return true;
    }

    /**
     * <p>Посылает интент указанному пакету с указанным юри</p>
     * @param pack Имя пакета приложения в который нужно отправить интент
     */
    public boolean appInstalled(String pack){
        //TO DO
        return true;
    }

}
