package app.com.appmodule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private AppModule appModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appModule = new AppModule(this);

        test();
    }

    public void test(){
        if(AppTests.test(appModule)){
            Log.d("MAIN_ACTIVITY", "WELL DONE");
        }else{
            Log.d("MAIN_ACTIVITY", "SOME ERRORS");
        }
    }
}
